package no.noroff.nicholas.attributestrategy;

import no.noroff.nicholas.attribute.Attribute;

public class MageAttributeBehaviour implements AttributeBehaviour {
    @Override
    public void setBase(Attribute attributes) {
        attributes = new Attribute(100,10,15,20);
    }

    @Override
    public void levelUp(Attribute attributes) {
        attributes.health += 10;
        attributes.strength += 3;
        attributes.dexterity += 5;
        attributes.intelligence += 10;
    }
}
