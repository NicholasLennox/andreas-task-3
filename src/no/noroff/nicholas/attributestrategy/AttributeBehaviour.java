package no.noroff.nicholas.attributestrategy;

import no.noroff.nicholas.attribute.Attribute;

public interface AttributeBehaviour {
    void setBase(Attribute attributes);
    void levelUp(Attribute attributes);
}
