package no.noroff.nicholas.attribute;

public class Attribute {
    public int health;
    public int strength;
    public int dexterity;
    public int intelligence;

    public Attribute() {
    }

    public Attribute(int health, int strength, int dexterity, int intelligence) {
        this.health = health;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }
}
