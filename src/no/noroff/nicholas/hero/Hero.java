package no.noroff.nicholas.hero;

import no.noroff.nicholas.attribute.Attribute;
import no.noroff.nicholas.attributestrategy.AttributeBehaviour;

public class Hero {
    private Attribute stats;
    private int level = 1;
    private AttributeBehaviour statsBehaviour;
    private HeroType heroType;

    public Hero(AttributeBehaviour statsBehaviour) {
        this.statsBehaviour = statsBehaviour;
        this.statsBehaviour.setBase(stats);
    }

    public void levelUp(){
        statsBehaviour.levelUp(stats);
    }
}
