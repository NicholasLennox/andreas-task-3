package no.noroff.nicholas.hero;

public enum HeroType {
    Mage,
    Warrior,
    Ranger
}
